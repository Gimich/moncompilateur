//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <vector>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {UNSIGNED_INT, BOOL, DOUBLE, CHAR};

TOKEN current;				// Current token
string variable = "";
FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
// AssignementStatement := Letter "=" Expression
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement "To" Expression "STEP" Number "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

// Display := Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Character := 'Letter'

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	string variable = lexer->YYText();
	if(!IsDeclared(lexer->YYText()))
	{
		Error("Variable non déclarée");
	}
	cout << "\tpush "<< variable <<endl;
	current=(TOKEN) lexer->yylex();
	return DeclaredVariables[variable];
}

TYPE Number(void){
	TYPE uneVariable = UNSIGNED_INT;
	string chiffre = lexer->YYText();
	if(chiffre.find('.') != string::npos)
	{
		double f= atof(chiffre.c_str()); // à l'adresse &f, il y a 8 octets qui contiennent le codage en norme IEEE754 du flottant 123.4
		long long unsigned int *i;     // i est un pointeur sur un espace de 8 octets sensé contenir un entier non signé en binaire naturel
		i= (long long unsigned int *) &f; // Maintenant, *i est un entier non signé sur 64 bits qui contient l'interprétation entière du codage IEEE754 du flottant 123.4 
		cout << "\tpush $"<<*i<<"\t# empile le flottant "<<f<<endl;
		uneVariable = DOUBLE;
	}
	else
	{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
	}
	return uneVariable;
}

string TypeToString(TYPE uneVariable)
{
	switch(uneVariable)
	{
		case UNSIGNED_INT:
			return "INT";
		case BOOL:
			return "BOOL";
		case DOUBLE:
			return "DOUBLE";
		case CHAR:
			return "CHAR";
		default:
			return "NULL";
	}
}


TYPE Expression(void);			// Called by Term() and calls Term()

// Display := Expression
void Display()
{
	current=(TOKEN) lexer->yylex();
	TYPE uneVariable = Expression();
	if (uneVariable == UNSIGNED_INT)
	{
		cout << "\tpop %rdx         # The value to be displayed\n";
		cout << "\tmovq $FormatString1, %rsi    # \"%llu\\n\"\n";
	}
	else if(uneVariable == DOUBLE)
	{
		cout << "\tpop %rdx         # The value to be displayed\n";
		cout << "\tmovq $FormatString2, %rsi    # \"%f\\n\"\n";
	}
	else if(uneVariable == CHAR)
	{
		cout << "\tpop %rdx         # The value to be displayed\n";
		cout << "\tmovq $FormatString3, %rsi    # \"%c\\n\"\n";
	}
	cout << "\tmovl    $1, %edi\n";
	cout << "\tmovl    $0, %eax\n";
	cout << "\tcall    __printf_chk@PLT\n";

}


TYPE Character()
{
	cout << "\tmov $0, %rax # On réinitialise rax\n";
	cout << "\tmovb $" << lexer->YYText() << ", %al\n";
	cout << "\tpush %rax \t#Met le char dans la pile\n";
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


TYPE Factor(void){
	TYPE uneVariable;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		uneVariable = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			uneVariable = Number();
	    else if(current==ID)
	    {
			uneVariable = Identifier();
	    }
		else if (current == CHA)
		{
			uneVariable = Character();
		}
		else
			Error("'(' ou chiffre ou lettre attendue");
	return uneVariable;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}


// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	OPMUL mulop;
	TYPE uneVariable;
	uneVariable = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();	// Save operator in local variable
		uneVariable = Factor();
		if(uneVariable == UNSIGNED_INT || uneVariable == BOOL)
		{
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(mulop){
				case AND:
					cout << "\tmul	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND"<<endl;	// store result
					break;
				case MUL:
					cout << "\tmul	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
					break;
				case DIV:
					cout << "\tmov $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
					break;
				case MOD:
					cout << "\tmov $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD"<<endl;		// store result
					break;
				default:
					Error("opérateur multiplicatif attendu");
			}
		}
		else if (uneVariable == DOUBLE)
		{
			cout << "\tfldl (%rsp) # Empile le flottant pointé par %rsp dans %st(0)"<<endl;	// get first operand
			cout << "\taddq $8, %rsp  # Dépilement de la pile générale." << endl;
			cout << "\tfldl (%rsp) # Empile le flottant pointé par %rsp dans %st(0)"<<endl;	// get second operand
			cout << "\taddq $8, %rsp  # Dépilement de la pile générale." << endl;
			switch(mulop){
				case AND:
					cout << "\tfmul	%st(0), st(1)"<<endl;
					cout << "\tsubq $8, %rsp" << endl;
					cout << "\tfstpl (%rsp)\t# AND"<<endl;	// store result
					break;
				case MUL:
					cout << "\tfmul	%st(0), st(1)"<<endl;
					cout << "\tsubq $8, %rsp" << endl;
					cout << "\tfstpl (%rsp)\t# AND"<<endl;	// store result
					break;
				case DIV:
					cout << "\tfdiv	%st(0), st(1)"<<endl;
					cout << "\tsubq $8, %rsp" << endl;
					cout << "\tfstpl (%rsp)\t# AND"<<endl;	// store result
					break;
				default:
					Error("opérateur multiplicatif attendu ou invalide");
			}
		}
		else
		{
			Error("Type invalide pour une opération arithmétique");
		}
	}
	cerr << "Fin term avec "<< lexer->YYText() <<"\n";
	return uneVariable;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE uneVariable;
	uneVariable = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		if(uneVariable != Term()){Error("Type différent dans un Term");}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return uneVariable;
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration()
{
	TYPE uneVariable = UNSIGNED_INT;
	std::vector<string> variables;
	if(current!=ID)
		Error("Un identificater était attendu");
	variables.push_back(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(strcmp(lexer->YYText(), ",") == 0)
	{
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		variables.push_back(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(strcmp(lexer->YYText(), ":") != 0)
	{
		Error("\":\" attendu.");
	}
	current=(TOKEN) lexer->yylex();
	if(strcmp(lexer->YYText(), "INT") == 0)
	{
		uneVariable = UNSIGNED_INT;
		for(vector<string>::iterator it = variables.begin(); it != variables.end(); it++)
		{
			cout << *it << ":\t.quad 0"<<endl;
		}
	}
	else if(strcmp(lexer->YYText(), "BOOL") == 0)
	{
		uneVariable = BOOL;
		for(vector<string>::iterator it = variables.begin(); it != variables.end(); it++)
		{
			cout << *it << ":\t.quad 0"<<endl;
		}
	}
	else if(strcmp(lexer->YYText(), "DOUBLE") == 0)
	{
		uneVariable = DOUBLE;
		for(vector<string>::iterator it = variables.begin(); it != variables.end(); it++)
		{
			cout << *it << ":\t.double 0.0"<<endl;
		}
	}
	else if(strcmp(lexer->YYText(), "CHAR") == 0)
	{
		uneVariable = CHAR;
		for(vector<string>::iterator it = variables.begin(); it != variables.end(); it++)
		{
			cout << *it << ":\t.byte 0"<<endl;
		}
	}
	else
	{
		Error("Type invalide lors de la déclaration de variables.");
	}
	current=(TOKEN) lexer->yylex();
	for(vector<string>::iterator it = variables.begin(); it != variables.end(); it++)
	{
		DeclaredVariables[*it] = uneVariable;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	if(strcmp(lexer->YYText(), "VAR") != 0)
	{
		Error("\"VAR\" attendu.");
	}
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current == SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!= DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE uneVariable;
	uneVariable = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		if(uneVariable != SimpleExpression()) {Error("Types différents lors d'une expression");}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		uneVariable = BOOL;
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
	}
	return uneVariable;
}

// AssignementStatement := Identifier ":=" Expression
TYPE AssignementStatement(void){
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	TYPE uneVariable = Expression();
	if(DeclaredVariables[variable] != uneVariable) {Error("AssignementStatement : " + TypeToString(uneVariable) + " != " + TypeToString(DeclaredVariables[variable]));}
	cout << "\tpop "<<variable<<endl;
	return uneVariable;
}


void Statement();

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement()
{
	int tag = ++TagNumber;
	cout << "IF" << tag << ":"; 
	current=(TOKEN) lexer->yylex();
	if(Expression() != BOOL) 
	{
		Error("Expression d'une condition non booléenne");
	}
	cout << "\tpop %rax\n";
	cout << "\tcmpq $0, %rax\n";
	cout << "\tje ELSE" << tag << endl;
	if(strcmp(lexer->YYText(), "THEN") == 0)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
		cout << "\tjmp EndIf" << tag << "\n";
		cout << "\tELSE"<< tag << ":\n";
		if(strcmp(lexer->YYText(), "ELSE") == 0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		cout << "EndIf" << tag << ":\n";
	}
	else {Error("\"THEN\" attendu.");}
}

//WhileStatement := "WHILE" Expression DO Statement
void WhileStatement()
{
	int tag = ++TagNumber;
	cout << "WHILE" << tag << ":\n";
	current=(TOKEN) lexer->yylex();
	if(Expression() != BOOL) 
	{
		Error("Expression d'une condition non booléenne");
	}
	cout << "\tpop %rax\n";
	cout << "\tcmpq $0, %rax\n";
	cout << "\tje EndWhile" << tag << endl;
	if(strcmp(lexer->YYText(), "DO") == 0)
	{
		current = (TOKEN) lexer->yylex();
		Statement();
		cout << "\tjmp WHILE" << tag << "\n";
		cout << "EndWhile" << tag << ":\n";
	}
	else {Error("\"DO\" attendu.");}
}

// ForStatement := "FOR" AssignementStatement "To" Expression "STEP" Number "DO" Statement 
void ForStatement()
{
	int nombre;
	int tag = ++TagNumber;
	current=(TOKEN) lexer->yylex();
	if(AssignementStatement() != UNSIGNED_INT)
	{
		Error("Variable non entière dans l'assignement d'un FOR");
	}
	cout << "FOR" << tag << ":\n";
	if(strcmp(lexer->YYText(), "TO") == 0)
	{
		current=(TOKEN) lexer->yylex();
		if(Expression() != BOOL)
		{
			Error("Expression d'un FOR non booléen");
		}
		cout << "\tpop %rax\n";
		cout << "\tcmpq $0, %rax\n";
		cout << "\tje EndFor" << tag << "\n"; //If Expression == FALSE
		if(strcmp(lexer->YYText(), "STEP") == 0)
		{
			current=(TOKEN) lexer->yylex();
			nombre = atoi(lexer->YYText());
			current=(TOKEN) lexer->yylex();
		}
		else
		{
			Error("\"STEP\" attendu");
		}
		if(strcmp(lexer->YYText(), "DO") == 0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\taddq $"<< nombre << ", " << variable << "\n";
			cout << "\tjmp FOR" << tag << "\n";
			cout << "EndFor" << tag << ":\n";
		}
		else
		{
			Error("\"DO\" attendu");
		}
	}
	else
	{
		Error("\"TO\" attendu");
	}

}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement()
{
	current=(TOKEN) lexer->yylex();
	Statement();
	while(current == SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(strcmp(lexer->YYText(), "END") == 0) 
		{
			current=(TOKEN) lexer->yylex();
			return;
		}
	else {Error("\"END\" attendu");}
}

// Statement := AssignementStatement
void Statement(void){
	if(current == ID)
	{
		AssignementStatement();
	}
	else if(strcmp(lexer->YYText(), "IF") == 0)
	{
		IfStatement();
	}
	else if(strcmp(lexer->YYText(), "WHILE") == 0)
	{
		WhileStatement();
	}
	else if(strcmp(lexer->YYText(), "FOR") == 0)
	{
		ForStatement();
	}
	else if(strcmp(lexer->YYText(), "BEGIN") == 0)
	{
		BlockStatement();
	}
	else if(strcmp(lexer->YYText(), "DISPLAY") == 0)
	{
		Display();
	}
	else {Error("Statement attendu");}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := DeclarationPart StatementPart
void Program(void){
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	cout << "FormatString1:    .string \"%llu\\n\"    # used by printf to display 64-bit unsigned integers\n";
	cout << "FormatString2:    .string \"%f\\n\"    # used by printf to display 64-bit double\n";
	cout << "FormatString3:    .string \"%c\\n\"    # used by printf to display 8-bit char\n";
	VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}