all:	test
clean:
		rm *.o *.s
		rm tokensiser.cpp
tokeniser.cpp:	tokeniser.l
		flex++ -d -otokeniser.cpp tokeniser.l
tokeniser.o:	tokeniser.cpp
		g++  -no-pie -fno-pie -c tokeniser.cpp
compilateur:	compilateur.cpp tokeniser.o
		g++  -no-pie -fno-pie -ggdb -o compilateur compilateur.cpp tokeniser.o
test:		compilateur test.p
		./compilateur <test.p >test.s
		gcc -no-pie -fno-pie -ggdb test.s -o test


