# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone git@framagit.org:jourlin/cericompiler.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master


**This version Can handle :**

* Program := [VarDeclarationPart] StatementPart
* VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} ".
* VarDeclaration := Ident {"," Ident} ":" Type
* StatementPart := Statement {";" Statement} "."
* Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
* AssignementStatement := Ident ":=" Expression
* IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
* WhileStatement := "WHILE" Expression DO Statement 
* ForStatement := "FOR" AssignementStatement "To" Expression "STEP" Number "DO" Statement

   Add "Number" to the var used in "AssignementStatement" defined with "STEP number" while "Expression" is true
* BlockStatement := "BEGIN" Statement { ";" Statement } "END"
* Ident := Letter{Letter | Digit}
* Display := Expression
* Expression := SimpleExpression [RelationalOperator SimpleExpression]
* SimpleExpression := Term {AdditiveOperator Term}
* Term := Factor {MultiplicativeOperator Factor}
* Factor := Number | ID | "(" Expression ")"| "!" Factor
* Number := Digit{Digit}
* Character := 'Letter'
* AdditiveOperator := "+" | "-" | "||"
* MultiplicativeOperator := "\*" | "/" | "%" | "&&"
* RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
* Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
* Letter := "a"|...|"z"
