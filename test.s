			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:    .string "%llu\n"    # used by printf to display 64-bit unsigned integers
FormatString2:    .string "%f\n"    # used by printf to display 64-bit double
FormatString3:    .string "%c\n"    # used by printf to display 8-bit char
a:	.byte 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	mov $0, %rax # On réinitialise rax
	movb $'a', %al
	push %rax 	#Met le char dans la pile
	pop a
	push a
	pop %rdx         # The value to be displayed
	movq $FormatString3, %rsi    # "%c\n"
	movl    $1, %edi
	movl    $0, %eax
	call    __printf_chk@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
